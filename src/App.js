import { useState } from "react";
import TodoInput from "./components/TodoInput";
import TodoList from "./components/TodoList";

function App() {
  const [tasks, setTaks] = useState([]);

  const updateTask = (newArray) => {
    setTaks(newArray)
  }

  const removeTask = (id) => {
    setTaks(tasks.filter(item => item.id !== id))
  }

  const handlerDoneTask = (id, done) => {
    setTaks(tasks.map(item => item.id === id ? {...item, done: done} : item))
  } 

  const updateName = (id, name) => {
    setTaks(tasks.map(item => item.id === id ? {...item, name: name} : item))
  }

  return (
    <div className="container">
      <TodoInput addNewTodo={(newTodo) => setTaks([...tasks, newTodo])} />
      <TodoList tasks={tasks} updateTask={updateTask} removeTask={removeTask} doneTask={handlerDoneTask} updateName={updateName} />
    </div>
  );
}

export default App;
