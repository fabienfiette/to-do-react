import { useEffect, useState } from "react";
import { PencilFill, TrashFill } from "react-bootstrap-icons";

function Task(props) {
  const [updateTaskName, setUpdateTaskName] = useState("");
  const [modify, setModify] = useState(false);

  const submitUpdateTask = (e) => {
    e.preventDefault()
    props.updateName(updateTaskName)
    setModify(false)
  }

  useEffect(() => {
    setModify(false)
  }, [props.data])

  return (
    <div className="card mb-3">
      <div className="card-body d-flex justify-content-between">
        {modify ? (
          <form className="input-group" onSubmit={submitUpdateTask}>
            <button type="submit" className="btn btn-primary input-group-prepend">Update</button>
            <input onChange={(e) => setUpdateTaskName(e.target.value)} value={updateTaskName} autoFocus type="text" className="form-control" placeholder="Update Todo" aria-label="Update Todo" aria-describedby="basic-addon1" />
          </form>
        ) : (
          <p className={props.data.done ? "card-text m-0 text-danger text-decoration-line-through" : "card-text m-0"}>{props.data.name}</p>
        )}
        <div className="d-flex align-items-center">
          <input className="form-form-check-input checkbox-wh ms-3" type="checkbox" checked={props.data.done} onChange={(e) => props.doneTask(e.target.checked)} />
          <PencilFill onClick={() => { setModify(true); setUpdateTaskName(props.data.name) }} color="#ffc107" className="ms-3 cursors-pointer" />
          <TrashFill onClick={() => { props.removeTask(); setModify(false) }} color="#dc3545" className="ms-3 cursors-pointer" />
        </div>
      </div>
    </div>
  );
}

export default Task;
