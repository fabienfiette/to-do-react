import { useState } from "react";

function TodoInput(props) {
  const [newTodo, setNewTodo] = useState("")

  const submitNewTodo = (e) => {
    e.preventDefault()

    setNewTodo("")

    const uuid = String.fromCharCode(Math.floor(Math.random() * 26) + 97)
      + Math.random().toString(16).slice(2)
      + Date.now().toString(16).slice(4);

    props.addNewTodo({ id: uuid, name: newTodo , done: false})
  }

  return (
    <>
      <h2 className="text-center m-3">TodoInput</h2>
      <form className="card" onSubmit={submitNewTodo}>
        <div className="card-body">
          <div className="input-group mb-3">
            <span className="input-group-text" id="icon-input">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-bookmark-fill" viewBox="0 0 16 16">
                <path d="M2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2z" />
              </svg>
            </span>
            <input onChange={(e) => setNewTodo(e.target.value)} value={newTodo} type="text" className="form-control" placeholder="New Todo" aria-label="New Todo" aria-describedby="icon-input" />
          </div>
          <div className="d-grid gap-2">
            <button type="submit" className="btn btn-primary">Add new task</button>
          </div>
        </div>
      </form>
    </>
  );
}

export default TodoInput;
