import { useState } from "react";
import Task from "./Task";

function TodoList(props) {
  const [filterTask, setFilterTask] = useState(null);

  const deleteDoneTask = () => {
    props.updateTask(props.tasks.filter(obj => !obj.done))
  }

  return (
    <>
      <h2 className="text-center m-3">TodoList</h2>
      <div className="d-flex justify-content-between flex-nowrap mb-5" role="toolbar" aria-label="Button group">
        <div className="btn-group w-100 me-5" role="group" aria-label="First group">
          <button onClick={() => setFilterTask(null)} type="button" className="btn btn-primary">All</button>
        </div>
        <div className="btn-group w-100 me-5" role="group" aria-label="Second group">
          <button onClick={() => setFilterTask(true)} type="button" className="btn btn-primary">Done</button>
        </div>
        <div className="btn-group w-100" role="group" aria-label="Third group">
          <button onClick={() => setFilterTask(false)} type="button" className="btn btn-primary">Todo</button>
        </div>
      </div>

      {
        props.tasks
          .filter(objFilter => {
            if ( filterTask === null ) {
              return objFilter
            }
            return filterTask ? objFilter.done : !objFilter.done
          })
          .map((obj, key) => (
            <Task
              key={key}
              data={obj}
              removeTask={() => props.removeTask(obj.id)}
              doneTask={(done) => props.doneTask(obj.id, done)}
              updateName={(name) => props.updateName(obj.id, name)}
            />
          ))
      }

      <div className="d-flex justify-content-between flex-nowrap mt-4" role="toolbar" aria-label="Button group">
        <div className="btn-group w-100 me-5" role="group" aria-label="First group">
          <button onClick={() => deleteDoneTask()} type="button" className="btn btn-danger">Delete done tasks</button>
        </div>
        <div className="btn-group w-100" role="group" aria-label="Second group">
          <button onClick={() => props.updateTask([])} type="button" className="btn btn-danger">Delete all tasks</button>
        </div>
      </div>
    </>
  );
}

export default TodoList;
